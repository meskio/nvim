vim.g.mapleader = ','

require('plugins')
require('lsp')
require('treesitter')
require('fmt')
require('git')
require('errors')
require('nvim-cmp')
require('nvim-telescope')
require('symbols')

--require('onedarkpro').load()
--vim.cmd[[colorscheme neon]]
--require('github-theme').setup()
vim.cmd[[colorscheme tokyonight]]

require("ibl").setup()

require('go').setup()

require('nvim-tree').setup()
vim.api.nvim_set_keymap('n', '<leader>t', ':NvimTreeToggle<CR>', {noremap = true})

require('lualine').setup()
