local signs = { Error = " ", Warning = " ", Hint = " ", Information = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

require("trouble").setup {
  -- your configuration comes here
  -- or leave it empty to use the default settings
  -- refer to the configuration section below
}

vim.api.nvim_set_keymap("n", "<leader>xx", "<cmd>Trouble<cr>",
  {silent = true, noremap = true}
)

vim.api.nvim_set_keymap("n", "<leader>xn", "<cmd>lua vim.diagnostic.goto_next()<cr>",
  {silent = true, noremap = true}
)

vim.api.nvim_set_keymap("n", "<leader>xp", "<cmd>lua vim.diagnostic.goto_prev()<cr>",
  {silent = true, noremap = true}
)
