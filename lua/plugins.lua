-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function()
	use 'wbthomason/packer.nvim'

	-- fuzzy finder
	use {
		'nvim-telescope/telescope.nvim',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	-- git decorations
	use {
		'lewis6991/gitsigns.nvim',
		requires = {
		  'nvim-lua/plenary.nvim'
		}
	}
	-- magit like
	-- use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' }

	-- debuger
	use 'mfussenegger/nvim-dap'
	
	-- display colors
	use 'norcalli/nvim-colorizer.lua'
	
	-- format code
	use 'mhartington/formatter.nvim'

	-- lsp config
	use 'neovim/nvim-lspconfig'

	-- lsp errors
	use {
	  "folke/trouble.nvim",
	  requires = "kyazdani42/nvim-web-devicons",
	}

	use {
		'simrat39/rust-tools.nvim',
		requires = {
			'nvim-lua/popup.nvim',
			'nvim-lua/plenary.nvim'
		}
	}

	-- completion
	use {
		'hrsh7th/nvim-cmp',
		requires = {
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-cmdline',
			'hrsh7th/nvim-cmp',
			'L3MON4D3/LuaSnip',
		},
	}

	-- colors
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate'
	}
	--use "projekt0n/github-nvim-theme"
	use "folke/tokyonight.nvim"
	--use 'olimorris/onedarkpro.nvim'
	--use 'rafamadriz/neon'
	--use 'marko-cerovac/material.nvim'

	-- tabline
	-- use {
	-- 	'romgrk/barbar.nvim',
	-- 	requires = {'kyazdani42/nvim-web-devicons'}
	-- }

	use "lukas-reineke/indent-blankline.nvim"


	use {
		'kyazdani42/nvim-tree.lua',
		requires = {
			'kyazdani42/nvim-web-devicons', -- optional, for file icon
		},
	}

	-- statusbar
	use {
		'nvim-lualine/lualine.nvim',
  		requires = {'kyazdani42/nvim-web-devicons', opt = true}
	}

	-- tagbar
	use 'preservim/tagbar'
	--use 'simrat39/symbols-outline.nvim'
	--use 'liuchengxu/vista.vim'
	--
	
	use {
		'ray-x/go.nvim',
		requires = {'ray-x/guihua.lua'}
	}
end)
