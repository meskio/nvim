require('telescope').setup{
}

local M = {}

function M.project_files()
  local ok = pcall(require('telescope.builtin').git_files)
  if not ok then require('telescope.builtin').find_files() end
end

vim.api.nvim_set_keymap('n', '<c-p>', "<cmd>lua require('nvim-telescope').project_files()<CR>", {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>ff', '<cmd>Telescope find_files<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>fb', '<cmd>Telescope buffers<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>fh', '<cmd>Telescope help_tags<cr>', {noremap = true})

return M
