-- tagbar
vim.api.nvim_exec([[
autocmd VimEnter * nested :call tagbar#autoopen(1)
let g:tagbar_type_javascript = {
    \ 'ctagstype' : 'JavaScript',
    \ 'kinds'     : [
        \ 'o:objects',
        \ 'f:functions',
        \ 'a:arrays',
        \ 's:strings'
    \ ]
    \ }
let g:rust_use_custom_ctags_defs = 1  " if using rust.vim
let g:tagbar_type_rust = {
  \ 'ctagstype' : 'rust',
  \ 'kinds' : [
      \ 'n:modules',
      \ 's:structures:1',
      \ 'i:interfaces',
      \ 'c:implementations',
      \ 'f:functions:1',
      \ 'g:enumerations:1',
      \ 't:type aliases:1:0',
      \ 'v:constants:1:0',
      \ 'M:macros:1',
      \ 'm:fields:1:0',
      \ 'e:enum variants:1:0',
      \ 'P:methods:1',
  \ ],
  \ 'sro': '::',
  \ 'kind2scope' : {
      \ 'n': 'module',
      \ 's': 'struct',
      \ 'i': 'interface',
      \ 'c': 'implementation',
      \ 'f': 'function',
      \ 'g': 'enum',
      \ 't': 'typedef',
      \ 'v': 'variable',
      \ 'M': 'macro',
      \ 'm': 'field',
      \ 'e': 'enumerator',
      \ 'P': 'method',
  \ },
\ }
]], true)

-- symbols outline
--vim.g.symbols_outline = {}
--vim.api.nvim_set_keymap('n', '<leader>s', ':SymbolsOutline<CR>', {noremap = true})

-- vista
--vim.api.nvim_set_keymap('n', '<leader>s', ':Vista!!<CR>', {noremap = true})
--vim.api.nvim_exec([[
--function! NearestMethodOrFunction() abort
--  return get(b:, 'vista_nearest_method_or_function', '')
--endfunction
--
--set statusline+=%{NearestMethodOrFunction()}
--
--" By default vista.vim never run if you don't call it explicitly.
--"
--" If you want to show the nearest function in your statusline automatically,
--" you can add the following line to your vimrc
--autocmd VimEnter * call vista#RunForNearestMethodOrFunction()
--
--let g:vista_icon_indent = ["╰─▸ ", "├─▸ "]
--
--let g:vista_default_executive = 'nvim_lsp'
--]], true)
