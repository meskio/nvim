require('formatter').setup({
	filetype = {
		rust = {
			-- Rustfmt
			function()
			  return {
			    exe = "rustfmt",
			    args = {"--emit=stdout"},
			    stdin = true
			  }
			end
		},
		go = {
			function()
			   return {
			     exe = "gofmt",
			     args = {},
			     stdin = true
			   }
			 end
		},
		javascript = {
			-- sudo npm install -g prettier
			function()
			   return {
			     exe = "prettier",
			     args = {"--stdin-filepath", vim.fn.fnameescape(vim.api.nvim_buf_get_name(0))},
			     stdin = true
			   }
			 end
		},
	}
})

vim.api.nvim_exec([[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *.rs,*.go,*.js FormatWrite
augroup END
]], true)
